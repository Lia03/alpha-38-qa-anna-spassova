import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;



public class BingTest {

    String expectedResult = "IT Career Start in 6 Months - Telerik Academy Alpha";

    @Test
    public void NavigateToBing(){
        System.setProperty("webdriver.chrome.driver","C:\\Users\\annsp\\Documents\\Drivers\\chromedriver_win32\\chromedriver.exe");

        WebDriver webDriver = new ChromeDriver();
        webDriver.get("http://bing.com");

        WebElement searchInput = webDriver.findElement(By.id("sb_form_q"));
        searchInput.sendKeys("Telerik Academy Alpha");
        WebElement searchButton = webDriver.findElement(By.xpath("//label[@for='sb_form_go']"));
        searchButton.click();

        WebElement actualResult = webDriver.findElement(By.xpath("//*[@id=\"b_results\"]/li[1]/div[1]/h2/a"));
        System.out.println(actualResult.getText());

        Assert.assertEquals(actualResult.getText(),expectedResult);

        webDriver.quit();

    }

    @Test
    public void NavigateToBing_List(){
        System.setProperty("webdriver.chrome.driver","C:\\Users\\annsp\\Documents\\Drivers\\chromedriver_win32\\chromedriver.exe");

        WebDriver webDriver = new ChromeDriver();
        webDriver.get("http://bing.com");

        WebElement searchInput = webDriver.findElement(By.id("sb_form_q"));
        searchInput.sendKeys("Telerik Academy Alpha");
        WebElement searchButton = webDriver.findElement(By.xpath("//label[@for='sb_form_go']"));
        searchButton.click();


        WebElement resultList = webDriver.findElement(By.id("b_results"));
//        System.out.println(resultList.getText());
        List<WebElement> results = resultList.findElements(By.tagName("li"));
        WebElement actualResult = results.get(0).findElement(By.xpath("//h2[@class=' b_topTitle']"));


        System.out.println(actualResult.getText());

        Assert.assertEquals(actualResult.getText(),expectedResult);

        webDriver.quit();

    }



}
