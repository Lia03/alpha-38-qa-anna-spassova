import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class GoogleTest {

    String expectedResult = "IT Career Start in 6 Months - Telerik Academy Alpha";

    @Test
    public void NavigateToGoogleChrome(){
        System.setProperty("webdriver.chrome.driver","C:\\Users\\annsp\\Documents\\Drivers\\chromedriver_win32\\chromedriver.exe");

        WebDriver webDriver = new ChromeDriver();
        webDriver.get("http://google.com");

        WebElement button = webDriver.findElement(By.xpath("//button[@id=\"L2AGLb\"]"));
        button.click();

        WebElement searchInput = webDriver.findElement(By.xpath("//input[@class='gLFyf gsfi']"));
        searchInput.sendKeys("Telerik Academy Alpha");
        WebElement searchButton = webDriver.findElement(By.xpath("//input [@class='gNO89b']"));
        searchButton.submit();

        WebElement firstResult = webDriver.findElement(By.xpath("//*[@id=\"rso\"]/div[1]/div/div[1]/div/a/h3"));
//        System.out.println(firstResult.getText());

        Assert.assertEquals(expectedResult,firstResult.getText());

        webDriver.quit();

    }
}
