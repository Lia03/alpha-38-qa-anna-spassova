package test.cases.jira;

import org.junit.Test;
import pages.jira.ProjectPage;

import static com.telerikacademy.testframework.Utils.getConfigPropertyByKey;

public class CreateBugTest extends BaseTest{

    @Test
    public void createBugTest() {
//        login();
        ProjectPage page = new ProjectPage(actions.getDriver());
        page.navigateToPage();
        page.createIssue("bug");

        page.assertProjectSelected(getConfigPropertyByKey(("jira.projectName")));
    }
}
