package pages.jira;

import org.openqa.selenium.WebDriver;
import static com.telerikacademy.testframework.Utils.getConfigPropertyByKey;

public class ProjectPage extends BaseJiraPage{

    public ProjectPage(WebDriver driver) {
        super(driver, "jira.projectUrl");
    }

    public void createIssue(String issueType){
        actions.waitForElementVisible("jira.projectPage.createButton");
        actions.clickElement("jira.projectPage.createButton");

        actions.waitForElementVisible("jira.projectPage.dropDownBox.issueTypeField");
        actions.clickElement("jira.projectPage.dropDownBox.issueTypeField");
        actions.waitForElementPresent("jira.projectPage." + issueType + ".issueType");
        actions.clickElement("jira.projectPage." + issueType + ".issueType");

        actions.waitForElementVisible("jira.projectPage.summaryField");
        actions.typeValueInField(getConfigPropertyByKey("jira." + issueType + ".summary"), "jira.projectPage.summaryField");

        actions.waitForElementVisible("jira.projectPage.descriptionField");
        actions.typeValueInField(getConfigPropertyByKey("jira." + issueType + ".description"), "jira.projectPage.descriptionField");

        actions.waitForElementVisible("jira.projectPage.dropDownBox.priority");
        actions.clickElement("jira.projectPage.dropDownBox.priority");
        actions.clickElement("jira.projectPage.high.priority");

        actions.waitForElementVisible("jira.projectPage.createIssueButton");
        actions.clickElement("jira.projectPage.createIssueButton");
    }

    public void linkStoryToBug(){
        actions.waitForElementVisible("jira.projectPage.issuesButton");
        actions.clickElement("jira.projectPage.issuesButton");
    }

    public void assertProjectSelected(String projectTitle){
        actions.waitForElementVisible("jira.projectPage.dropDownBox.project");
        actions.assertElementAttribute("jira.projectPage.dropDownBox.project", "innerText", projectTitle);
    }
}
